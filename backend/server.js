const express = require("express");
const dotenv = require("dotenv").config();
const connectDB = require('./config/db')
connectDB()
const {errorHandler} = require('./middleware/errorMiddleware')
const colors = require("colors");
const port = 4000;
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use("/api/landlords", require('./routes/landlordRoutes'));
app.use("/api/admins", require('./routes/adminRoutes'));
app.use("/api/users", require('./routes/userRoutes'));
app.use("/api/propertys", require('./routes/propertyRoutes'));
app.use(errorHandler)
app.listen(port, () => console.log(`Server started on port ${port}`));
