// To recreate the admin Auth middleware so as to give me the token for the registering administrators
const jwt = require("jsonwebtoken");
const asyncHandler = require("express-async-handler");
const Admin = require("../models/adminModel");
const adminProtect = asyncHandler(async (req, res, next) => {
  let token;
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer")
  ) {
    try {
      token = req.headers.authorization.split(" ")[1];
      const decoded = jwt.verify(token, process.env.JWT_SECRET);
      // Get the admin from the token
      req.admin = await Admin.findById(decoded.id).select("-password");
      next();
    } catch (error) {
      console.log(error);
      res.status(401)
      throw new Error("Admin Not Authorized")
    }
  }
  if(!token){
    res.status(401)
    throw new Error("Admin Not authorized ,no token")
  }
});
module.exports = {adminProtect}
