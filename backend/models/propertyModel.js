const mongoose = require("mongoose");
const propertySchema = mongoose.Schema(
  {
    admin: {
      type: mongoose.Schema.Types.ObjectId,
      // required: true,
      ref: "Admin",
    },
    name: {
      type: String,
      required: [true, "Please Name"],
    },
    bedrooms: {
      type: Number,
      required: [true, "How many Bedrooms"],
    },
    bathrooms: {
      type: Number,
      required: [true, "How many bathrooms"],
    },
    units: {
      type: Number,
      required: [true, "How many Units are available "],
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model("Property",propertySchema)
