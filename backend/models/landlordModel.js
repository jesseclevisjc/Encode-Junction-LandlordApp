const mongoose = require("mongoose");
const landlordSchema = mongoose.Schema(
  {
    admin: {
      type: mongoose.Schema.Types.ObjectId,
      // required: true,
      ref: "Admin",
    },
    firstName: {
      type: String,
      required: [true, "Please add your first name"],
    },
    lastName: {
      type: String,
      required: [true],
    },
    phoneNumber: {
      type: String,
      required: [true],
    },
    email: {
      type: String,
      required: [true],
    },
    location: {
      type: String,
      required: [true, "Please add a District /Town /City"],
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model("Landlord", landlordSchema);
