const express = require("express");
const router = express.Router();
const {
  getProperty,
  setProperty,
  updateProperty,
  deleteProperty,
} = require("../controllers/propertyController");
// const {protect} = require('../middleware/authMiddleware')
// Best to have your functions in your controller
router.route('/').get(getProperty).post(setProperty)
router.route('/:id').delete(deleteProperty).put(updateProperty)
module.exports = router;
