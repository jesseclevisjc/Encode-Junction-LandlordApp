const express = require("express");
const router = express.Router();
const {
  registerAdmin,
  getMe,
  loginAdmin,
} = require("../controllers/adminController");
const { adminProtect } = require("../middleware/adminAuthMiddleware");
router.post("/", registerAdmin);
router.post("/loginAdmin", loginAdmin);
router.get("/me", adminProtect, getMe);
module.exports = router;

// check 38:42 to use login to get the token
