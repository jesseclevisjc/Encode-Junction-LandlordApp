const express = require("express");
const router = express.Router();
// const {} = require('../controllers/landlord Controller')
const {
  getLandlord,
  setLandlord,
  updateLandlord,
  deleteLandlord,
} = require("../controllers/landlordController");
const {adminProtect} = require("../middleware/adminAuthMiddleware")
// const {protect} = require('../middleware/authMiddleware')
// Best to have your functions in your controller
router.route('/').get(getLandlord).post(setLandlord)
router.route('/:id').delete(deleteLandlord).put(updateLandlord)
module.exports = router;
