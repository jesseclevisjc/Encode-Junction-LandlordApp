const asyncHandler = require("express-async-handler");
const Landlord = require("../models/landlordModel");
const Admin = require("../models/adminModel");
// @desc Get Landlords
// @route GET /api/landlords
// @access Private
const getLandlord = asyncHandler(async (req, res) => {
  const landlords = await Landlord.find();
  res.status(200).json(landlords);
});
// @desc Set Landlords
// @route POST /api/landlords
// @access Private
const setLandlord = asyncHandler(async (req, res) => {
  const { firstName, lastName, phoneNumber, email, location } = req.body;
  if (!firstName || !lastName || !phoneNumber || !email || !location) {
    res.status(400);
    throw new Error("Please add all the fields ");
  }
  // res.status(200).json({ message: "set the landlord" });
  // Setting up a landlord

  const landlord = await Landlord.create({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    phoneNumber: req.body.phoneNumber,
    email: req.body.email,
    location: req.body.location,
    admin:req.body.Admin
    // admin: req.Admin.id,
  });
  res.status(200).json(landlord);
});

// @desc Update Landlords
// @route PUT /api/landlords
// @access Private
const updateLandlord = asyncHandler(async (req, res) => {
  const landlord = await Landlord.findById(req.params.id);
  if (!landlord) {
    res.status(400);
    throw new Error("Landlord not found");
  }
  const admin = await Admin.findById(req.admin.id);
  // Check for admin
  if (!admin) {
    res.status(401);
    throw new Error("Admin not found");
  }
  // Make sure logged in user matches the landlord admin though i might not need this


  const updatedLandlord = await Landlord.findByIdAndUpdate(
    req.params.id,
    req.body,
    {
      new: true,
    }
  );
  res.status(200).json(updatedLandlord);

  res.status(200).json({ message: `Update landlord ${req.params.id}` });
});
// @desc Delete Landlords
// @route DELETE /api/landlords
// @access Private
const deleteLandlord = asyncHandler(async (req, res) => {
  const landlord = await Landlord.findById(req.params.id);
  if (!landlord) {
    res.status(400);
    throw new Error("Landlord not found");
  }
  const deletedLandlord = await Landlord.findByIdAndDelete(req.params.id);
  res.status(200).json({ id: req.params.id });
});
module.exports = {
  getLandlord,
  setLandlord,
  updateLandlord,
  deleteLandlord,
};
