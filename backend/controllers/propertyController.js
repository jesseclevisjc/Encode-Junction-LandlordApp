const asyncHandler = require("express-async-handler");
const Property = require("../models/propertyModel");

// @desc Get proprerty
// @route GET /api/property
// @access Private
const getProperty = asyncHandler(async (req, res) => {
  const property = await Property.find();
  res.status(200).json(property);
});
// @desc Set Landlords
// @route POST /api/landlords
// @access Private
const setProperty = asyncHandler(async (req, res) => {
  const { name, bedrooms, bathrooms, units} = req.body;
  if (!name || !bedrooms || !bathrooms | !units) {
    res.status(400);
    throw new Error("Please add all the fields ");
  }
  // res.status(200).json({ message: "set the landlord" });
  // Setting up a landlord

  const property = await Property.create({
    name:req.body.name,
    bedrooms:req.body.bedrooms,
    bathrooms:req.body.bathrooms,
    units:req.body.units,
    admin:req.body.Admin
  });
  res.status(200).json(property);
});

// @desc Update Landlords
// @route PUT /api/landlords
// @access Private
const updateProperty = asyncHandler(async (req, res) => {
  const property = await Property.findById(req.params.id);
  if (!property) {
    res.status(400);
    throw new Error("Property not found");
  }
  const admin = await Admin.findById(req.admin.id);
  // Check for admin
  if (!admin) {
    res.status(401);
    throw new Error("Admin not found");
  }
  // Make sure logged in user matches the landlord admin though i might not need this
  const updatedProperty = await Property.findByIdAndUpdate(
    req.params.id,
    req.body,
    {
      new: true,
    }
  );
  res.status(200).json(updatedProperty);

  res.status(200).json({ message: `Update property ${req.params.id}` });
});
// @desc Delete Property
// @route DELETE /api/property
// @access Private
const deleteProperty = asyncHandler(async (req, res) => {
  const property = await Property.findById(req.params.id);
  if (!property) {
    res.status(400);
    throw new Error("Property not found");
  }
  const deletedProperty = await Property.findByIdAndDelete(req.params.id);
  res.status(200).json({ id: req.params.id });
});
module.exports = {
  getProperty,
  setProperty,
  updateProperty,
  deleteProperty,
};
