
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const asyncHandler = require("express-async-handler");
const Admin = require("../models/adminModel");

// @desc Register new admin
// @route POST /api/admin
// @access Public
const registerAdmin = asyncHandler(async (req, res) => {
  const { name, email, password } = req.body;
  if (!name || !email || !password) {
    res.status(400);
    throw new Error("Please fill in all the fields");
  }
  // Check if User Exists
  const adminExists = await Admin.findOne({ email });
  if (adminExists) {
    res.status(400);
    throw new Error("Administrator already exists");
  }
  // Hashing the password
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(password, salt);
  // Create an admin
  const admin = await Admin.create({
    name,
    email,
    password: hashedPassword,
  });
  if (admin) {
    res.status(201).json({
      _id: admin.id,
      name: admin.name,
      email: admin.email,
      token: generateToken(admin._id),
    });
  } else {
    res.status(400);
    throw new Error("Invalid Admin data");
  }
});
// @desc Authenticate an admin
// @route POST /api/admin/login
// @access Public
const loginAdmin = asyncHandler(async (req, res) => {
  const { email, password } = req.body;
  const admin = await Admin.findOne({ email });
  if (admin && (await bcrypt.compare(password, admin.password))) {
    res.json({
      _id: admin.id,
      name: admin.name,
      email: admin.email,
      token: generateToken(admin._id),
      
    });
  } else {
    res.status(400);
    throw new Error("Invalid Admin data");
  }
  res.json({message:"Logged In User"})
});
// @desc Get admin data
// @route POST /api/admin/me
// @access Public
const getMe = asyncHandler(async (req, res) => {
  const { _id, name, email } = await Admin.findById(req.admin.id);
  // when you hit this route you should get this info back 
  res.status(200).json({
    id: _id,
    name,
    email,
  });
});

// generate JWT
const generateToken = (id) => {
  return jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: "30d",
  });
};
module.exports = {
  registerAdmin,
  loginAdmin,
  getMe,
};
