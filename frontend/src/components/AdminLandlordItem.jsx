import { useDispatch } from "react-redux";
import {deleteLandlord} from "../features/landlords/landlordSlice"

function AdminLandlordItem ({landlord}){
  const dispatch = useDispatch()
  return (
    <div className="goal">
      <h2>{landlord.firstName} </h2>
      <div>{new Date(landlord.createdAt).toLocaleString('en-US')}</div>
      <button onClick={()=>dispatch(deleteLandlord(landlord._id))} className="close">x</button>
      
    </div>
  )
}
export default AdminLandlordItem