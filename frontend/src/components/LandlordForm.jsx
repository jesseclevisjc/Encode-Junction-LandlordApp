import {useEffect, useState} from "react"
import { useDispatch, useSelector } from "react-redux"
import { createLandlord,reset } from "../features/landlords/landlordSlice"
import Spinner from "./Spinner"
import { useNavigate } from "react-router-dom"
function LandlordForm(){
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const [formData,setFormData] = useState({
    firstName:"",
    lastName:"",
    email:"",
    phoneNumber:"",
    location:"",
    // eslint-disable-next-line no-dupe-keys
    email:"",
  })
  const {firstName,lastName,email,phoneNumber,
  location}= formData
  const {isLoading} = useSelector((state)=>state.landlord)
  useEffect(()=>{
    dispatch(reset())
  })

  const onChange= (e)=>{
    setFormData((prevState)=>({
      ...prevState,[e.target.name]:e.target.value
    }))
  }
  const onClick = (e)=>{
    // Take me back to the AdminDashboard to view the created
    // navigate('/adminDashboard')
    
  }
  const onSubmit = (e) =>{
    e.preventDefault()
    const landlordData={
      firstName,
      lastName,
      email,
      phoneNumber,
      location,
    }
    dispatch(createLandlord(landlordData))
    navigate('/')
    
    if(isLoading){
      return<Spinner />
    }
    navigate('/')
  }
  return (
    <section className="form">
      <form onSubmit={onSubmit} >
        <div className="form-group">
          <label htmlFor="text" className="landlordForm">Register Landlord</label>
          <input type="text" name="firstName" id="firstName" value={firstName} placeholder="Enter firstName"  onChange={onChange} />
          <input type="text" name="lastName" value={lastName} id="text"placeholder="Enter Last Name"  onChange={onChange} />
          <input type="text" name="phoneNumber" value={phoneNumber} id="phoneNumber" placeholder="Enter Phone number"onChange={onChange} />
          <input type="email" name="email" id="email" value={email} placeholder="Enter Email" onChange={onChange} />
          <input type="text" name="location" id="location" value={location} placeholder="Enter Location" onChange={onChange} />
        </div>
        <div className="form-group">
          <button className="btn btn-block" type="submit" onClick={onClick}> Add Landlord</button>
        </div>
      </form>
    </section>
  ) 
}
export default LandlordForm