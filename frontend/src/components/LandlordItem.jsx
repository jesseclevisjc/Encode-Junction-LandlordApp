
function LandlordItem ({landlord}){

  return (
    <div className="goal">
      <h3>{landlord.firstName} {landlord.lastName}  </h3>
      <h4>Tel:{}</h4>
      <h4>Located at {landlord.location}</h4>
      <h6>{new Date(landlord.createdAt).toLocaleString('en-US')}</h6>
     
      
    </div>
  )
}
export default LandlordItem