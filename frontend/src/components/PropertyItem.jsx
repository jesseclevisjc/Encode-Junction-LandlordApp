// import { useDispatch } from "react-redux";
function PropertyItem({property}){
  return(
    <div className="goal">
      <h3>{property.name}</h3>
      <h4>Bedrooms: {property.bedrooms}</h4>
      <h4>Bathrooms: {property.bathrooms}</h4>
      <h4>Units: {property.units}</h4>
      <h6>{new Date(property.createdAt).toLocaleString('en-US')}</h6>
 
      {/* <h2>{property}</h2> */}
    </div>
  )
}
export default PropertyItem