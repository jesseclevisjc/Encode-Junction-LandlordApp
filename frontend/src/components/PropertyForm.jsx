
import  { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { createProperty,resetProperty } from '../features/propertyAuth/propertySlice'
import Spinner from './Spinner'
import AdminDashboard from '../pages/AdminDashboard'
import { useNavigate } from 'react-router-dom'
// import AdminDashboard from '../pages/AdminDashboard'

function PropertyForm() {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const [formData,setFormData] = useState({
    name:"",
    bedrooms:"",
    bathrooms:"",
    units:"",
  })
  const {name,bedrooms,bathrooms,units}= formData
  const{isLoading} =useSelector((state)=>state.property)
  useEffect(()=>{
    dispatch(resetProperty())
  })
  const onChange=(e)=>{
    setFormData((prevState)=>({
      ...prevState,[e.target.name]:e.target.value
    }))
  }
  const onClick=()=>{
    return <AdminDashboard />
  }
  const onSubmit =(e)=>{
    e.preventDefault()
    const propertyData={
      name,bedrooms,bathrooms,units
    }
    dispatch(createProperty(propertyData))
    navigate('/')
    if(isLoading){
      return <Spinner />
    }
  }
  return (
    <section className="form">
      <form onSubmit={onSubmit}>
        <div className="form-group">
          <label htmlFor="text" className="propertyForm">Register Property</label>
          <input type="text" name='name' id='name' value={name}  onChange={onChange} placeholder='Enter name' />
          <input type="text" name='bedrooms' id='bedrooms' value={bedrooms}  onChange={onChange} placeholder='Enter bedrooms' />
          <input type="text" name='bathrooms' id='bathrooms' value={bathrooms}  onChange={onChange} placeholder='Enter Bathrooms' />
          <input type="text" name='units' id='units' value={units}  onChange={onChange} placeholder='Enter Units'/>
        </div>
        <div className="form-group">
          <button className="btn btn-block" type='submit' onClick={onClick}>Add Property</button>
        </div>
      </form>
    </section>
  )
}

export default PropertyForm