import axios from "axios"
// import { config } from "dotenv"
const API_URL = '/api/propertys/'
// Create new property
// Removed the token from the createProperty function
const createProperty= async (propertyData,token)=>{
  const config ={
    headers:{
      Authorization:`Bearer ${token}`
    }
  }
  const response = await axios.post(API_URL,propertyData,config)
  return response.data
}
// Get Properties
const getPropertys =async (token) =>{
  const config ={
    headers:{
      Authorization:`Bearer ${token}`
    }
  }
  const response = await axios.get(API_URL,config)
  return response.data
}
// Delete Property
const deleteProperty = async(propertyId,token)=>{
  const config= {
    headers:{
      Authorization:`Bearer ${token}`
    }
  }
  const response = await axios.delete(API_URL +propertyId, config)
  return response.data
}
const propertyService ={
  createProperty,getPropertys,deleteProperty
}
export default propertyService