import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import propertyService from "./propertyService";
const initialState = {
  propertys: [],
  isError: false,
  isSuccess: false,
  isLoading: false,
  message: "",
};
// create new property
export const createProperty = createAsyncThunk(
  // Changed the create 
  "propertys/create",
  async (propertyData, thunkAPI) => {
    // I am gonna try to work without the token
    try {
      // const token = thunkAPI.getState().adminAuth.admin.token;
      return await propertyService.createProperty(propertyData);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);
// Get Properties
export const getPropertys = createAsyncThunk(
  "propertys/getAll",
  async (_, thunkAPI) => {
    try {
      const token = thunkAPI.getState().adminAuth.admin.token;
      return await propertyService.getPropertys(token);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);
// Delete the Property
export const deleteProperty = createAsyncThunk(
  "propertys/delete",
  async (id, thunkAPI) => {
    try {
      const token = thunkAPI.getState().adminAuth.admin.token;
      return await propertyService.deleteProperty(id, token);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const propertySlice = createSlice({
  name: "property",
  initialState,
  reducers: {
    resetProperty: (state) => initialState,
  },
  extraReducers: (builder) => {
    builder
      .addCase(createProperty.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(createProperty.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.propertys.push(action.payload);
      })
      .addCase(createProperty.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
      })
      .addCase(getPropertys.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getPropertys.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.propertys = action.payload;
      })
      .addCase(getPropertys.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
      })
      .addCase(deleteProperty.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(deleteProperty.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.propertys = state.propertys.filter(
          (property) => property._id !== action.payload.id
        );
      })
      .addCase(deleteProperty.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
      });
  },
});
export const { resetProperty } = propertySlice.actions;
export default propertySlice.reducer;
