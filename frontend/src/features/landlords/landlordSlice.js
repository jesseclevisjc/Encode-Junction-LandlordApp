import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import landlordService from "./landlordService";
const initialState = {
  landlords: [],
  isError: false,
  isSuccess: false,
  isLoading: false,
  message: "",
};
// Create a Landlord
export const createLandlord = createAsyncThunk(
  "landlords/create",
  async (landlordData, thunkAPI) => {
    try {
      // thunk Api enables us to get the state from the auth which gives access to the token
      // Change the user to the landlord
      // const token = thunkAPI.getState().authAdmin.admin.token;
      return await landlordService.createLandlord(landlordData);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);
// Get Landlords using a token
// This might need editing to make it get any Landlord
export const getLandlords = createAsyncThunk(
  "landlords/getAll",
  async(_, thunkAPI)=>{
    try {
      // Get the token from the state from auth
      const token = thunkAPI.getState().adminAuth.admin.token
      return await landlordService.getLandlords(token)
      
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);

    }
  }
);

// Delete Landlord
export const deleteLandlord = createAsyncThunk(
  "landlords/delete",
  async(id,thunkAPI)=>{
    try {
      const token = thunkAPI.getState().adminAuth.admin.token
      return await landlordService.deleteLandlord(id,token)
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);

    }
  }
) 

export const landlordSlice = createSlice({
  name: "landlord",
  initialState,
  reducers: {
    reset: (state) => initialState,
  },
  extraReducers: (builder) => {
    builder
      .addCase(createLandlord.pending, (state) => {
        state.isLoading = true;
      })

      .addCase(createLandlord.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.landlords.push(action.payload);
      })
      .addCase(createLandlord.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
      })
      .addCase(getLandlords.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getLandlords.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.landlords=action.payload
      })
      .addCase(getLandlords.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
      })
      .addCase(deleteLandlord.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(deleteLandlord.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.landlords=state.landlords.filter((landlord)=>landlord._id !==action.payload.id)
      })
      .addCase(deleteLandlord.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
      });
  },
});
export const { reset } = landlordSlice.actions;
export default landlordSlice.reducer;
