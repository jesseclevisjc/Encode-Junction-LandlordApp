import axios from "axios";
const API_URL = '/api/landlords/'
// Create a Landlord

// This is where my error is being found , do i really need a token in this project or can i go on withut one
const createLandlord = async(landlordData,token)=>{
  const config = {
    headers:{
      Authorization:`Bearer ${token}`
    }
  }
  const response = await axios.post(API_URL,landlordData,config)
  return response.data
}
// Get Landlords
const getLandlords = async(token)=>{
  const config = {
    headers:{
      Authorization:`Bearer ${token}`,
    },
  }
  const response = await axios.get(API_URL,config)

  return response.data
}

// Delete Landlord
const deleteLandlord = async(landlordId,token)=>{
  const config = {
    headers:{
      Authorization: `Bearer ${token}`,
    },
  }
  const response = await axios.delete(API_URL + landlordId, config)
  return response.data
}
const landlordService ={
  createLandlord,getLandlords,deleteLandlord
}
export default landlordService