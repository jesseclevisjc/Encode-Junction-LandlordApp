import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import adminService from "./adminService";
// Get admin from localstorage
const admin = JSON.parse(localStorage.getItem("admin"));

const initialState = {
  admin: admin ? admin : null,
  isError: false,
  isSuccess: false,
  isLoading: false,
  message: "",
};
// Register Admin
export const registerAdmin = createAsyncThunk(
  "admin/registerAdmin",
  async (admin, thunkAPI) => {
    try {
      return await adminService.registerAdmin(admin);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);
// Login Administrator
export const loginAdmin = createAsyncThunk(
  "admin/loginAdmin",
  async(admin,thunkAPI)=>{
    try {
      return await adminService.loginAdmin(admin)
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
)
export const logoutAdmin = createAsyncThunk("admin/logout",async () =>{
  await adminService.logoutAdmin
})
export const adminSlice = createSlice({
  name:"admin",
  initialState,
  reducers:{
    resetAdmin: (state) => {
      state.isLoading = false;
      state.isError = false;
      state.isSuccess = false;
      state.message = "";
    },
  },
  extraReducers:(builder)=>{
    builder
      .addCase(registerAdmin.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(registerAdmin.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.admin = action.payload;
      })
      .addCase(registerAdmin.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
        state.admin = null;
      })
      .addCase(loginAdmin.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(loginAdmin.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.admin = action.payload;
      })
      .addCase(loginAdmin.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
        state.admin = null;
      })
      .addCase(logoutAdmin.fulfilled,(state)=>{
        state.admin=null
      });
  }
});
export const {resetAdmin} = adminSlice.actions;
export default adminSlice.reducer
