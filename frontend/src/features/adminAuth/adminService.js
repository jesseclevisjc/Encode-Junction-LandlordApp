import axios from "axios";
const API_URL = "/api/admins/";
// Register an administrator
const registerAdmin = async (adminData) => {
  const response = await axios.post(API_URL, adminData);
  if (response.data) {
    localStorage.setItem("admin", JSON.stringify(response.data));
  }
  return response.data;
};
// Login Administrator
const loginAdmin = async (adminData) => {
  const response = await axios.post(API_URL + "loginAdmin", adminData);
  if (response.data) {
    localStorage.setItem("admin", JSON.stringify(response.data));
  }
  return response.data;
};
// Logout Administrator
const logoutAdmin = () => {
  localStorage.removeItem("admin");
};
const adminService = {
  registerAdmin,
  loginAdmin,
  logoutAdmin,
};
export default adminService;
