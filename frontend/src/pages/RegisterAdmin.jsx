import {useState,useEffect} from "react"
import {useSelector,useDispatch} from 'react-redux'
import {toast} from'react-toastify'
import Spinner from "../components/Spinner"
import { FaUser } from "react-icons/fa"
import { registerAdmin, resetAdmin} from '../features/adminAuth/adminSlice'
import { useNavigate } from "react-router-dom"
function RegisterAdmin() {
  const [formData,setFormData]=useState({
    name:"",
    email:"",
    password:"",
    password2:"",
  })
  const {name,email,password,password2} = formData

  const navigate = useNavigate()
  const dispatch = useDispatch()
  const {admin,isLoading,isError,isSuccess ,message}=useSelector(
    ( state )=>state.adminAuth
   )
    useEffect(()=>{
      if(isError){
        toast.error(message)

      }
      if (isSuccess ||admin){
        navigate('/adminDashboard')
      }
      dispatch(resetAdmin())

    },[admin,isError,isSuccess,message,navigate,dispatch])

  const onChange = (e)=>{
    setFormData((prevState)=>({
      ...prevState,
      [e.target.name]:e.target.value
    }))
  }
  const onSubmit = (e) =>{
    e.preventDefault()
    if (password !==password2){
      toast.error('Passwords do not match ')
    }else{
      const adminData ={
        name, email, password
      }
      dispatch(registerAdmin(adminData))
    }
  }
  if(isLoading){
    return <Spinner/>
  }

  return <>
    <section className="Heading">
      <h1>
        <FaUser/>Register Administrator</h1>
      <p>
        Please create an account
      </p>
    </section>
    <section className="form">
      <form onSubmit={onSubmit}>
        <div className="form-group">

        <input type="text" className="form-control" id="name" name="name" value={name} placeholder="Enter your name" onChange={onChange}></input>
        </div>
        <div className="form-group">

        <input type="email" className="form-control" id="email" name="email" value={email} placeholder="Enter your email" onChange={onChange}></input>
        </div>
        <div className="form-group">

        <input type="password" className="form-control" id="password" name="password" value={password} placeholder="Enter your password" onChange={onChange}></input>
        </div>
        <div className="form-group">

        <input type="password" className="form-control" id="password2" name="password2" value={password2} placeholder="Confirm your password" onChange={onChange}></input>
        </div>
        <div className="form-group">
          <button type="submit" className="btn btn-block">
            Submit
          </button>
        </div>

      </form>

    </section>
    </>
  
}

export default RegisterAdmin