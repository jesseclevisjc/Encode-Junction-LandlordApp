import {useState,useEffect} from "react"
import {useSelector,useDispatch} from 'react-redux'
import {toast} from'react-toastify'
import Spinner from "../components/Spinner"
import { FaSignInAlt } from "react-icons/fa"
import { useNavigate } from "react-router-dom"
import { loginAdmin,resetAdmin} from '../features/adminAuth/adminSlice'
function LoginAdmin() {
  const [formData,setFormData]=useState({
    
    email:"",
    password:"",
    
  })
  const {email,password,} = formData
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const {admin,isLoading,isError,isSuccess ,message}=useSelector(
    ( state )=>state.adminAuth
   )
   useEffect(()=>{
    if(isError){
      toast.error(message)

    }
    if (isSuccess ||admin){
      navigate('/adminDashboard')
    }
    dispatch(resetAdmin())

  },[admin,isError,isSuccess,message,navigate,dispatch])

  const onChange = (e)=>{
    setFormData((prevState)=>({
      ...prevState,
      [e.target.name]:e.target.value
    }))
  }
  const onSubmit = (e) =>{
    e.preventDefault()
    const adminData={
      email,
      password
    }
    dispatch(loginAdmin(adminData))
  }
  if (isLoading){
    return<Spinner />
  }
  return <>
    <section className="Heading">
      <h1>
        <FaSignInAlt/>Login administrator</h1>
      <p>
         Login Here
      </p>
    </section>
    <section className="form">
      <form onSubmit={onSubmit}>   
        <div className="form-group">
        <input type="email" className="form-control" id="email" name="email" value={email} placeholder="Enter your email" onChange={onChange}></input>
        </div> 
        <div className="form-group">
        <input type="password" className="form-control" id="password" name="password" value={password} placeholder="Enter your password" onChange={onChange}></input>
        </div>       
        <div className="form-group">
          <button type="submit" className="btn btn-block">Submit</button>
        </div>
      </form>
    </section>
    </>
  
}

export default LoginAdmin