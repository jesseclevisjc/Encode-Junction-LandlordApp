import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import AdminLandlordItem from '../components/AdminLandlordItem'
import { Link, useNavigate } from 'react-router-dom'
import { getLandlords } from '../features/landlords/landlordSlice'
import { getPropertys } from '../features/propertyAuth/propertySlice'
import { FaSignOutAlt } from 'react-icons/fa'
import { logoutAdmin, resetAdmin } from '../features/adminAuth/adminSlice'
// import {BrowserRouter,Router,Routes,Route} from "react-router-dom"

function AdminDashboard() {
  const dispatch= useDispatch()
  const navigate = useNavigate()
  const {admin} = useSelector((state)=>(state.adminAuth))
  const onLogout =()=>{
    dispatch(logoutAdmin())
    dispatch(resetAdmin())
    navigate("/")
  }
  const {landlords,isLoading,isError,message} = useSelector((state)=>(state.landlord))
  useEffect(()=>{
    if(isError){
      console.log(message);
    }
    if(!admin){
      navigate('/loginAdmin')
    }
    dispatch(getLandlords())
    dispatch(getPropertys())
    // return()=>{
    //   dispatch(reset())
    //   dispatch(resetProperty())
    // }
  },[admin,isError,isLoading,dispatch,message,navigate])
  return (
  <>
    {admin?(
      <li>
        <button className="btn" onClick={onLogout}>
          <FaSignOutAlt /> Logout
        </button>
      </li>
    ):(
      <>
       <h3>the</h3>
      </>
    )}
    <h1>Admin Dashboard</h1>
    <h2>Welcome Admin { admin && admin.name}</h2>
    <div className="btn">
      <h3>Administrator Fuctionalities</h3>
    </div>
    <header className='header'>
        <div className="logo">
          Create here/Register
        </div>
        <ul>
          <li>
            <Link to="/landlordForm">
              Landlord
            </Link>
          </li>
          <li>
            <Link to="/propertyForm">
              Property
            </Link>
          </li>
          <li>
            <Link to="/setCommission">
              Commission
            </Link>
          </li>
        </ul>
      </header>
      
      <section className="content">
        <div className="landlordSection">
        <section className="content">
        Landlord Section
        {landlords.length > 0?(
          <div className="goals">
            {landlords.map((landlord)=>(
              <AdminLandlordItem key={JSON.stringify(landlord._id)}  landlord={landlord} />
            ))}
          </div>
        ):(
          <>No landlords Present</>
        )}
      </section>

        </div>
        <div className="propertySection">

        </div>
      </section>
    
   
  </>
  )
}

export default AdminDashboard