import {useEffect} from 'react'
import {useNavigate} from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import LandlordItem from '../components/LandlordItem'
import { getLandlords,reset } from '../features/landlords/landlordSlice'
import Spinner from "../components/Spinner"
import PropertyItem from '../components/PropertyItem'
import {getPropertys,resetProperty} from "../features/propertyAuth/propertySlice"

function Dashboard() {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  // Looks like i might have to destructure the components as i cant be declaring two different functions in the same place
  // On this dashboard we need the auth that gets user but not admin 
  const {user} =useSelector((state)=>state.auth)
  // const {} = useSelector((state)=>state.adminAuth)
  const {landlords,isLoading,isError,message} = useSelector((state)=>(state.landlord))
  const {propertys} = useSelector((state)=>(state.property))
  useEffect(()=>{
    if(isError){
      console.log(message);
    }
    if(!user){
      navigate('/login')
    }
    dispatch(getLandlords())
    dispatch(getPropertys())
    return ()=>{
      dispatch(reset())
      dispatch(resetProperty())
    }
  },[user,navigate,isError,message,dispatch])
  if(isLoading){
    return <Spinner />
  }

  return (
    <>
      <section className="heading">
        {/* <h2>Welcome this is the trial div</h2> */}
        <h1>Welcome { user && user.name }</h1>
        <p>User Dashboard</p>
      </section> 
      <section className="content">
        Landlord Section
        {landlords.length > 0?(
          <div className="goals">
            {landlords.map((landlord)=>(
              <LandlordItem key={JSON.stringify(landlord._id)}  landlord={landlord} />
            ))}
          </div>
        ):(
          <>No landlords Present</>
        )}
      </section>
      <section className="content">
        Property Section
        {propertys.length > 0?(
          <div className="goals">
            {propertys.map((property)=>(
              <PropertyItem key={JSON.stringify(property._id)} property={property}  />
            ))}
          </div>
        ):(
          <>No Properties Present</>
        )}
      </section>
    </>
  )
}

export default Dashboard