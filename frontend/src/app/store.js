import { configureStore } from "@reduxjs/toolkit";
import authReducer from "../features/auth/authSlice";
import landlordReducer from "../features/landlords/landlordSlice";
import propertyReducer from "../features/propertyAuth/propertySlice";
import adminAuthReducer from "../features/adminAuth/adminSlice";
// We add our reducer from what is called a slice
export const store = configureStore({
  reducer: {
    auth: authReducer,
    adminAuth: adminAuthReducer,
    property: propertyReducer,
    landlord: landlordReducer,
  },
});
