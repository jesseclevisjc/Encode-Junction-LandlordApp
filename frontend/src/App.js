import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Dashboard from "./pages/Dashboard";
import Login from "./pages/Login";
import Register from "./pages/Register";
import RegisterAdmin from "./pages/RegisterAdmin";
import LoginAdmin from "./pages/LoginAdmin";
import Header from "./components/Header";
import AdminDashboard from "./pages/AdminDashboard";
import LandlordForm from "./components/LandlordForm";
import PropertyForm from "./components/PropertyForm";
function App() {
  return (
    <>
      <Router>
        <div className="container">
          <Header />
          <Routes>
            <Route path='/' element={<Dashboard />} /> 
            <Route path='/login' element={<Login />} />
            <Route path='/register' element={<Register />} />
            <Route path="/registerAdmin" element={<RegisterAdmin/>}  />
            <Route path="/loginAdmin" element={<LoginAdmin />}/>
            <Route path="/adminDashboard" element={<AdminDashboard/>}/> 
            <Route path="/landlordForm" element={<LandlordForm/>}/> 
            <Route path="/propertyForm" element={<PropertyForm/>}/> 

          </Routes>
        </div>
      </Router>
    </>
  );
}

export default App;
